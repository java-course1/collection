package demo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

public class ListClass {

    void arrayListMethod () {
        ArrayList arrayList = new ArrayList();
        arrayList.add(12);
        arrayList.add(4);
        arrayList.add(5);

        for (Object integer : arrayList) {
            System.out.println(integer);
        }
    }

    void vectorMethod () {
        Vector<Integer> list = new Vector();
        list.add(12);
        list.add(13);
        list.add(4);

        list.remove(new Integer(13));
        list.set(0, 5);
        System.out.println(list.size());

        for (Integer integer: list) {
            System.out.println(integer);
        }
    }

    void linkedListMethod () {
        LinkedList linkedList = new LinkedList();
        linkedList.add(12);
        linkedList.add("new Object");
        linkedList.add(12);

        for (Object object : linkedList) {
            System.out.println(object);
        }
    }

    public static void main(String[] args) {
        ListClass listClass = new ListClass();
//        listClass.arrayListMethod();
//        listClass.vectorMethod();
        listClass.linkedListMethod();
    }
}
