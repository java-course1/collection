package demo;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {

    void hashSetMethod () {
        Set set = new HashSet();
        HashSet hashSet = new HashSet();
        hashSet.add(12);
        hashSet.add("new String");
        hashSet.add(12.9);
        hashSet.add(12);

        hashSet.remove(12);

        for (Object object : hashSet) {
            System.out.println(object);
        }
    }

    void treeSetMethod () {
        TreeSet treeSet = new TreeSet();
        treeSet.add(12);
        treeSet.add(4.0);
        treeSet.add(5.8);
        treeSet.add(8.0);

        for (Object object : treeSet) {
            System.out.println(object);
        }
    }

    public static void main(String[] args) {
        SetDemo setDemo = new SetDemo();
//        setDemo.hashSetMethod();
        setDemo.treeSetMethod();
    }
}
