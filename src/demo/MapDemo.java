package demo;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {

    void hashMapMethod () {
        Map<Object, Object> map = new HashMap();
        map.put(1, "first element");
        map.put("key 2", 3);
        map.put(2, 4.6);

        for (Map.Entry<Object, Object> entry: map.entrySet()) {
            System.out.println(entry.getKey());
        }
    }


    public static void main(String[] args) {
        MapDemo mapDemo = new MapDemo();
        mapDemo.hashMapMethod();
    }
}
