package book;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private Scanner scanner = new Scanner(System.in);
    private List list = new ArrayList();
    private BookManipulation bookManipulation = new BookManipulation();

    public void displayOptions () {
        char option = 0;

        do {
            System.out.println("\n1. Add book");
            System.out.println("2. Show books");
            System.out.println("3. Remove book by id");
            System.out.println("4. Update book by id");
            System.out.println("5. Search book by id");
            System.out.println("6. Exit program\n");

            System.out.print("Option: ");
            option = scanner.nextLine().charAt(0);

            switch (option) {
                case '1': // add book
                    bookManipulation.addBook(list);
                    break;
                case '2': // show books
                    bookManipulation.showBooks(list);
                    break;
                case '3': // remove book
                    System.out.print("Enter Book ID to remove : ");
                    String id = scanner.nextLine();

                    bookManipulation.removeBook(list, bookManipulation.getBook(list, Integer.parseInt(id)));
                    break;
                case '4': // update book
                    System.out.print("Enter Book ID to update : ");
                    String updateId = scanner.nextLine();
                    bookManipulation.updateBook(list, bookManipulation.getBook(list, Integer.parseInt(updateId)));
                    break;
                case '5': // search book
                    System.out.print("Enter Book Title to Search : ");
                    String searchTitle = scanner.nextLine();
                    List searchedList = bookManipulation.searchBooks(list, searchTitle);
                    bookManipulation.showBooks(searchedList);
                    break;
                case '6': // exit
                    System.exit(0);
                    break;
            }
        } while (option != '6');
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.displayOptions();
    }
}
