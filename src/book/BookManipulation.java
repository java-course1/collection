package book;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BookManipulation {
    private Scanner scanner = new Scanner(System.in);

    public List addBook (List list) {
        System.out.print("Enter ID : ");
        int id = scanner.nextInt();

        System.out.print("Enter Title: ");
        String title = scanner.nextLine();

        System.out.print("Enter Published Year : ");
        int year = scanner.nextInt();

        list.add(new Book(id, title, year));
        return list;
    }

    public void showBooks (List list) {
        if (list.isEmpty()) {
            System.out.println("There is no book");
        } else {
            for (Object object : list) {
                System.out.println(object);
            }
        }

    }

    public List removeBook (List list, Book book) {
            if (list.remove(book)) {
                System.out.println("removed successfully");
            } else {
                System.out.println("removed unsuccessfully");
            }
            return list;

    }

    public List updateBook (List list, Book book) {
        if (book != null) {
            int index = list.indexOf(book);
            Book book1 = new Book(book.getId(), "updated title", 2020);
            list.set(index, book1);
            System.out.println("updated successfully");
        } else {
            System.out.println("updated unsuccessfully");
        }

        return list;
    }

    public List searchBooks (List<Book> list, String searchTitle) {
        List list1 = new ArrayList();
        boolean isFound = false;

        for (Book book : list) {
            if (book.getTitle().equals(searchTitle)) {
                list1.add(book);
                isFound = true;
            }
        }

        if (isFound) {
            System.out.println("Search found !!!");
        } else {
            System.out.println("Search not found");
        }


        return list1;
    }

    public Book getBook (List<Book> list, int bookId) {

        for (Book book: list) {
            if (book.getId() == bookId) {
                return book;
            }
        }
        return null;
    }
}
